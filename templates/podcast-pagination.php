<?php
/**
 * Creates our pagination between pages
 *
 * @package		WP Web Apps
 * @subpackage	Church Core
 * @since		1.0.0
 *
 */

 
 	the_posts_pagination( array(
		'mid_size'			 => 2,
		'prev_text'			 => '',
		'next_text'			 => '',
		'screen_reader_text' => __( 'Pagination', 'church-core' )
	) );