<?php
/**
 * Speaker Archive
 *
 * Creates an archive of all podcasts attributed to the same speaker
 *
 * @package		WP Web Apps
 * @subpackage	Church Core
 * @since		1.0.0
*/ 

get_header(); ?>

<div class="wrap">

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			
			<header class="entry-header">
				<h1 class="entry-title"><?php single_term_title(); ?></h1>
			</header>
				
			<?php the_archive_description( '<div class="chuch-core-taxonomy-description">', '</div>' ); ?>
			
			<div class="church-core-container podcast-list">
				
				<?php do_action('church_core_before_pod_archive'); ?>
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php echo do_shortcode('[podcast-item]'); ?>
		
				<?php endwhile; else: ?>

					<p><?php _e( 'Sorry, no podcasts in this series yet.', 'church-core' ); ?></p>
				
				<?php endif; ?>
			
			</div><!-- .podcast-list -->
				
			<?php echo do_shortcode('[podcast-pagination]'); ?>

			<?php do_action('church_core_after_pod_archive'); ?>
			
		</main>
	</div>
    <?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>