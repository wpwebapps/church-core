<?php
/**
 * Podcast Item
 *
 * This creates one entry in a podcast table
 *
 * @package		WP Web Apps
 * @subpackage	Church Core
 * @since 		1.0.0
 *
 */

 ?>

	<div class="podcast-episode cc-row" id="podcast-episode-<?php the_ID(); ?>">

		<div class="cc-column cc-title-meta">
			<div class="cc-episode-title">
				<a href="<?php the_permalink(); ?>" rel="bookmark">
					<?php the_title( '<h2>', '</h2>' ); ?>
				</a>
			</div>
			<div class="episode-speaker-series">
				<?php echo get_the_term_list( get_the_ID(), 'speaker', '<span class="cc-episode-speaker">', '', '</span>' ) ?>
				<?php echo get_the_term_list( get_the_ID(), 'series', '<span class="cc-episode-series">', '', '</span>' ) ?>
				<time class="cc-episode-date">
					<?php the_date(); ?>
				</time>
			</div>
		</div>

		<div class="cc-column is-narrow episode-actions">
				
			<?php if ( get_post_meta($post->ID, 'message_audio', $single = true) != "" ) { ?>

				<a class="listen" href="<?php the_permalink(); ?>">
					<span class="cc-icon-helper">
						<svg width="20" height="20" viewBox="0 0 16 16">
							<path fill="#000000" d="M4.5 9h-1v7h1c0.275 0 0.5-0.225 0.5-0.5v-6c0-0.275-0.225-0.5-0.5-0.5z"></path>
							<path fill="#000000" d="M11.5 9c-0.275 0-0.5 0.225-0.5 0.5v6c0 0.275 0.225 0.5 0.5 0.5h1v-7h-1z"></path>
							<path fill="#000000" d="M16 8c0-4.418-3.582-8-8-8s-8 3.582-8 8c0 0.96 0.169 1.88 0.479 2.732-0.304 0.519-0.479 1.123-0.479 1.768 0 1.763 1.304 3.222 3 3.464v-6.928c-0.499 0.071-0.963 0.248-1.371 0.506-0.084-0.417-0.129-0.849-0.129-1.292 0-3.59 2.91-6.5 6.5-6.5s6.5 2.91 6.5 6.5c0 0.442-0.044 0.874-0.128 1.292-0.408-0.259-0.873-0.435-1.372-0.507v6.929c1.696-0.243 3-1.701 3-3.464 0-0.645-0.175-1.249-0.479-1.768 0.31-0.853 0.479-1.773 0.479-2.732z"></path>
						</svg>
					</span>
				</a>

				<?php } ?>

				<?php if ( ( get_post_meta($post->ID, 'video_embed', $single = true ) != "" ) || ( get_post_meta($post->ID, "message_video", $single = true ) != "" ) ) { ?>

				<a class="watch" href="<?php the_permalink(); ?>">
					<span class="cc-icon-helper">
						<svg width="20" height="20" viewBox="0 0 16 16">
							<path fill="#000000" d="M15.331 2.502c-2.244-0.323-4.724-0.502-7.331-0.502s-5.087 0.179-7.331 0.502c-0.43 1.683-0.669 3.543-0.669 5.498s0.239 3.815 0.669 5.498c2.244 0.323 4.724 0.502 7.331 0.502s5.087-0.179 7.331-0.502c0.43-1.683 0.669-3.543 0.669-5.498s-0.239-3.815-0.669-5.498zM6 11v-6l5 3-5 3z"></path>
						</svg>
					</span>
				</a>

				<?php } ?>

				<?php if ( get_post_meta($post->ID, 'message_notes', $single = true ) != ""  ) { ?>

				<a class="notes" download="<?php the_title(); ?>" href="<?php echo get_post_meta($post->ID, 'message_notes', true); ?>">
					<span class="cc-icon-helper">
						<svg width="20" height="20" viewBox="0 0 16 16">
							<path fill="#000000" d="M14.341 3.579c-0.347-0.473-0.831-1.027-1.362-1.558s-1.085-1.015-1.558-1.362c-0.806-0.591-1.197-0.659-1.421-0.659h-7.75c-0.689 0-1.25 0.561-1.25 1.25v13.5c0 0.689 0.561 1.25 1.25 1.25h11.5c0.689 0 1.25-0.561 1.25-1.25v-9.75c0-0.224-0.068-0.615-0.659-1.421zM12.271 2.729c0.48 0.48 0.856 0.912 1.134 1.271h-2.406v-2.405c0.359 0.278 0.792 0.654 1.271 1.134zM14 14.75c0 0.136-0.114 0.25-0.25 0.25h-11.5c-0.135 0-0.25-0.114-0.25-0.25v-13.5c0-0.135 0.115-0.25 0.25-0.25 0 0 7.749-0 7.75 0v3.5c0 0.276 0.224 0.5 0.5 0.5h3.5v9.75z"></path>
							<path fill="#000000" d="M11.5 13h-7c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h7c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z"></path>
							<path fill="#000000" d="M11.5 11h-7c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h7c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z"></path>
							<path fill="#000000" d="M11.5 9h-7c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h7c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z"></path>
						</svg>
					</span>
				</a>

			<?php } ?>

		</div>

	</div><!-- .podcast-episode -->