<?php
/**
 * Creates our search form for podcast episodes
 *
 * @package		WP Web Apps
 * @subpackage	Church Core
 * @since		1.0.0
 *
 *
 */?>

<form method="get" class="searchform clearfix" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	<input type="search" class="field" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php _e('Search For Messages', 'church-core'); ?>" />
	<input type="submit" class="button btn" name="submit" id="search-submit" value="<?php esc_attr_e( 'Search', 'church-builder' ); ?>" />
	<input type="hidden" name="post_type" value="podcast" />
</form>