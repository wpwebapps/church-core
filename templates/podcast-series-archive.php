<?php

/**
 * Series Archive
 * 
 * Creates layout for displaying all series.  Inserted into page via shortcode.
 *
 * @package		WordPress
 * @subpackage	Church Core
 * @since		1.0.0
 *
 */

?>

<div class="cc-podcast-series-list">
		
    <?php foreach (get_terms('series', array(
            'orderby'	=> 'id',
            'order'	=> 'DESC'
        )) as $list) : ?>
    
        <div class="cc-podcast-series-item">
                        
            <a href="<?php echo get_term_link($list->slug, 'series'); ?>">
            
                <?php
                    
                    $term_id = $list->term_id;
                    
                    if ( get_term_meta( $term_id, 'image', true ) != '' ) {
                        
                        $image_url = esc_url( get_term_meta( $term_id, 'image', true ) );
                        
                        echo '<img class="cc-podcast-series-thunbnail" src="'.$image_url.'" />';
                        
                    } else {
                    
                        $image_url = esc_url( CHURCH_CORE_PLUGIN_DIR_URL .'assets/images/series-placeholder.png' );
                        
                        echo '<img class="cc-podcast-series-thunbnail" src="'.$image_url.'" />';
                    
                    }
                    
                ?>
        
                <div class="series-title"><h3><?php echo $list->name; ?></h3></div>
            
            </a>
        
        </div>
        
    
    <?php endforeach; ?>
    
</div><!-- .podcast-series -->