<?php

get_header(); ?>

<div class="wrap">

<div id="primary" class="content-area podcast-episode-<?php the_ID(); ?>">

	<main id="main" class="site-main">

		<?php if ( get_post_meta( $post->ID, 'video_embed', $single = true ) != "" ) { ?>
					
			<div class="church-core-video cc-embedded-video"><?php echo get_post_meta( $post->ID, 'video_embed', true ); ?></div>
						
		<?php } else if ( get_post_meta( $post->ID, 'message_video', $single = true ) != "" ) { ?>
			
			<div class="church-core-video cc-shortcode-video">
				
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
				
				<?php

					$raw_file = get_post_meta( $post->ID, 'message_video', true );
					$attachment_id = church_core_get_image_id( $raw_file );
					$file_meta = get_post_meta( $attachment_id, '_wp_attachment_metadata', true );
					$width = $file_meta['width'];
					$height = $file_meta['height'];

					echo do_shortcode( '[video src="'.get_post_meta(get_the_ID(), 'message_video', true).'" width="'.$width.'" height="'.$height.'" poster="'.$image[0].'"]' ); 
				
				?>

				<script>
					$(function() {
						$('div.cc-shortcode-video .mejs-overlay-loading').closest('.mejs-overlay').addClass('load');

						var $video = $('div.cc-shortcode-video video');
						var vidWidth = $video.attr('width');
						var vidHeight = $video.attr('height');

						$(window).resize(function() {
							var targetWidth = $(this).width(); 
							$('div.cc-shortcode-video, div.cc-shortcode-video .mejs-container').css('height', Math.ceil(vidHeight * (targetWidth / vidWidth)));
						}).resize();
					});
				</script>
			
			</div>
				
		<?php } else if ( has_post_thumbnail() ) { ?>
							
			<?php the_post_thumbnail( 'podcast-single', array( 'class' => 'series-hero' ) ); ?>
						
		<?php } else { ?>
			
			<?php

				foreach ( ( get_the_terms( $post->ID, 'series' ) ) as $term ) { 
				
					$term_list = $term->term_id. '';
					
					$term_title = esc_html( $term->name. '' );
					
					$image_id = get_term_meta( $term_list, 'image', true );
					
					$image_url = esc_url( $image_id );
					
					echo '<img class="series-hero" src="'.$image_url.'" alt="'.$term_title.'" />';
				
				}
							
			?>
		
		<?php } ?>
		
		<?php while (have_posts()) : the_post(); ?>
		
		<header class="entry-header">
		
			<h1 class="entry-title"><?php the_title(); ?></h1>
			
		</header><!-- .entry-header -->
		
		<p class="episode-details">
			
			<time class="episode-date">
				<?php the_date(); ?>
			</time>
			
			<?php echo get_the_term_list( get_the_ID(), 'speaker', '<span class="speaker">', '', '</span>' ) ?>
			
			<?php if ( get_post_meta($post->ID, "message_notes", $single = true ) != "" ) : ?>
		
				<a class="download-notes" download="<?php the_title(); ?>" href="<?php echo get_post_meta(get_the_ID(), 'message_notes', true); ?>"><?php _e( 'Notes', 'church-core' ); ?></a>
			
			<?php endif; ?>
			
		</p>

		<?php if ( get_post_meta( $post->ID, 'message_audio', $single = true ) != "" ) { ?>

			<div class="cc-message-audio">
			
				<?php echo do_shortcode( '[audio src="'.get_post_meta(get_the_ID(), 'message_audio', true).'"]' ); ?>
			
			</div>
		
		<?php } ?>
							
		<?php the_content(); ?>
		
		<?php endwhile; ?>

	</main>
	
</div>

<?php get_sidebar(); ?>

</div>
<?php get_footer(); ?>