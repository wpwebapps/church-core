<?php 

/**
 * Podcast Archive
 * 
 * Displays all published podcasts with pagination
 *
 * @package		WordPress
 * @subpackage	Church Core
 * @since		1.0.0
 *
 */

get_header(); ?>

<div class="wrap">

    <div id="primary" class="content-area">
		<main id="main" class="site-main">

            <h1 class="entry-title">
                <?php post_type_archive_title(); ?>
            </h1>
            
            <?php do_action('church_core_before_pod_archive'); ?>

            <?php while ( have_posts() ) : the_post(); ?>

                <?php echo do_shortcode('[podcast-item]'); ?>

            <?php endwhile; ?>

            <?php echo do_shortcode('[podcast-pagination]'); ?>

            <?php do_action('church_core_after_pod_archive'); ?>
        
        </main>
    </div>

    <?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>