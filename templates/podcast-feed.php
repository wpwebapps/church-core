<?php 
/*
 * Podcast Feed
 * 
 * This file creates a iTunes compatible RSS feed from our podcast episodes.
 * It can contain either audio or video.  Each media type should get its own feed.
 *
 * @package		WP Web Apps
 * @subpackage	Church Core
 * @since		1.0.0
 *
 */
header("Content-Type: application/rss+xml; charset=UTF-8");
echo '<?xml version="1.0" encoding="UTF-8"?>';
?><rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">
<?php while (have_posts()) : the_post(); ?>
<channel>

<title><?php echo get_post_meta( $post->ID, "podcast_title", TRUE ); ?></title>

<link><?php echo get_post_meta( $post->ID, "podcast_url", TRUE ); ?></link>

<language><?php bloginfo( 'language' ); ?></language>

<copyright>&#x2117; &amp; &#xA9; <?php the_time( 'Y' ); ?> <?php bloginfo( 'name' ); ?></copyright>

<itunes:subtitle><?php echo get_post_meta( $post->ID, "podcast_subtitle", TRUE ); ?></itunes:subtitle>

<itunes:author><?php echo get_post_meta( $post->ID, "podcast_author", TRUE ); ?></itunes:author>

<itunes:summary><?php echo get_post_meta( $post->ID, "podcast_summary", TRUE ); ?></itunes:summary>

<description><?php echo get_post_meta( $post->ID, "podcast_summary", TRUE ); ?></description>

<itunes:owner>

	<itunes:name><?php echo get_post_meta( $post->ID, "podcast_owner", TRUE ); ?></itunes:name>
	
	<itunes:email><?php echo get_post_meta( $post->ID, "podcast_email", TRUE ); ?></itunes:email>

</itunes:owner>

<itunes:image href="<?php echo get_post_meta( $post->ID, "podcast_art", TRUE ); ?>" />

<itunes:category text="<?php echo get_post_meta( $post->ID, "podcast_category", TRUE ); ?>">

	<itunes:category text="<?php echo get_post_meta( $post->ID, "podcast_subcategory", TRUE ); ?>"/>

</itunes:category>

<itunes:explicit><?php echo get_post_meta( $post->ID, "podcast_explicit", TRUE ); ?></itunes:explicit>

<?php $feed_id = get_the_id(); ?>

<?php endwhile; ?>

<?php

	$per_page = get_post_meta( $post->ID, 'per_page', TRUE );

	if  ($per_page == '' ) {
		$per_page = 52;
	}

	if ( get_post_meta( $feed_id, 'feed-type', TRUE ) == 'audio' ) {
	
		$feed_results = new WP_Query( 
			array( 
				'post_type'			=> 'podcast', 
				'posts_per_page'	=> $per_page, 
				'orderby'			=> 'date', 
				'order'				=> 'DESC',
				'no_found_rows'		=> true,
				'meta_query'             => array(
						array(
							'key'       => 'message_audio',
							'value'     => '',
							'compare'   => '!=',
						),
					),
				) 
			);
	
	} else if ( get_post_meta( $feed_id, 'feed-type', TRUE ) == 'video' ) {
	
		$feed_results = new WP_Query( 
			array( 
				'post_type'			=> 'podcast', 
				'posts_per_page'	=> $per_page, 
				'orderby'			=> 'date', 
				'order'				=> 'DESC',
				'no_found_rows'		=> true,
				'meta_query'             => array(
					array(
						'key'       => 'message_video',
						'value'     => '',
						'compare'   => '!=',
					),
				),
				) 
			);
	
	}

?>

<?php while ( $feed_results->have_posts() ) : $feed_results->the_post(); ?>

	<item>
	
		<title><?php echo get_the_title( $post->ID ); ?></title>
		
		<itunes:author><?php
		     $terms_as_text = get_the_term_list( $post->ID, 'speaker', '', ', ', '' ) ;
		     echo strip_tags( $terms_as_text );			
		?></itunes:author>
		
		<itunes:subtitle></itunes:subtitle>
		
		<itunes:summary><?php the_excerpt_rss(); ?></itunes:summary>
		
		<?php
			if ( has_post_thumbnail() ) {
				$thumb_id = get_post_thumbnail_id();
				$thumb_url_array = wp_get_attachment_image_src( $thumb_id, 'podcast-feed', true );
				$thumb_url = $thumb_url_array[0];
			} else {
				$thumb_url = get_post_meta( $feed_id, 'podcast_art', TRUE );
			}
		?>
		
		<itunes:image href="<?php echo $thumb_url; ?>" />

		<pubDate><?php church_core_rss_date( strtotime( $post->post_date_gmt ) ); ?></pubDate>

		<?php 
		
			if ( get_post_meta( $feed_id, 'feed-type', TRUE ) == 'video' ) { 
				
				$raw_file = get_post_meta( $post->ID, "message_video", TRUE );

				$raw_type = 'video/mpeg';

			} else { 
				
				$raw_file = get_post_meta( $post->ID, "message_audio", TRUE );
				$raw_type = 'audio/mpeg';
			
			}
			
			$attachment_id = church_core_get_image_id( $raw_file );

			if ( $attachment_id == '' ) {

				$readable_duration = '00:00:000';
				$file_size = '43397038';
				$file_type = $raw_type;

			} else {

				$file_meta = get_post_meta( $attachment_id, '_wp_attachment_metadata', true );
				$readable_duration = $file_meta['length_formatted'];
				$file_size = $file_meta['filesize'];
				$file_type = $file_meta['mime_type'];

			}
		 
		?>

		<enclosure url="<?php echo $raw_file; ?>" length="<?php echo $file_size ?>" type="<?php echo $file_type; ?>" />		
		<guid><?php echo $raw_file; ?></guid>

		<itunes:duration><?php echo $readable_duration; ?></itunes:duration>
		
		<itunes:keywords><?php echo get_post_meta( $post->ID, "message_keywords", TRUE ); ?></itunes:keywords>
	
	</item>
		  
	<?php endwhile; ?> 
  
</channel>
</rss>