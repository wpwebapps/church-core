<?php

/**
 * Adds support for our podcasting
 * post type and the message/speaker taxonomies
 *
 * @package		WordPress
 * @subpackage	Church Core
 * @since		1.0.0
 *
 */

// Register podcast Post Type
	function church_core_podcast_register() {
	
	if ( post_type_exists( 'podcast' ) ) {
		return;
	}

	//$permalinks = get_option( 'church_core' );
	$permalinks = get_option('church_core_options');
	
	if ( $permalinks['podcast'] != 'podcast_permalink' ) {
		$podcast_slug = $permalinks['podcast_permalink'];
	} else {
		$podcast_slug = 'podcast';
	}
	
	$podcast_params = array(
		'public' 				=> true,
		'show_ui' 				=> true,
		'hierarchical' 			=> false,
		'publicly_queryable' 	=> true,
		'has_archive' 			=> true,
		'exclude_from_search' 	=> false,
		'show_in_nav_menus'		=> false,
		'capability_type'		=> 'post',
		'menu_icon'				=> 'dashicons-microphone',
		'rewrite' 				=> array( 'slug' => $podcast_slug, 'with_front' => false ),
		'supports' 				=> array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments', 'custom-fields' ),
		'show_in_rest'			=> true,
		'labels' 				=> array(
			'name'                  => _x( 'Podcast', 'church_core' ),
			'singular_name'         => _x( 'Podcast', 'church_core' ),
			'menu_name'             => __( 'Podcast', 'church_core' ),
			'name_admin_bar'        => __( 'Podcast', 'church_core' ),
			'archives'              => __( 'Podcast Archives', 'church_core' ),
			'parent_item_colon'     => __( 'Podcast Parent', 'church_core' ),
			'all_items'             => __( 'All Podcasts', 'church_core' ),
			'add_new_item'          => __( 'Add New Podcast', 'church_core' ),
			'add_new'               => __( 'Add Podcast', 'church_core' ),
			'new_item'              => __( 'New Podcast', 'church_core' ),
			'edit_item'             => __( 'Edit Podcast', 'church_core' ),
			'update_item'           => __( 'Update Podcast', 'church_core' ),
			'view_item'             => __( 'View Podcast', 'church_core' ),
			'search_items'          => __( 'Search Podcasts', 'church_core' ),
			'not_found'             => __( 'No podcasts', 'church_core' ),
			'not_found_in_trash'    => __( 'No podcasts in the trash', 'church_core' ),
			'featured_image'        => __( 'Podcast Image', 'church_core' ),
			'set_featured_image'    => __( 'Set podcast image', 'church_core' ),
			'remove_featured_image' => __( 'Remove podcast image', 'church_core' ),
			'use_featured_image'    => __( 'Use as podcast image', 'church_core' ),
			'insert_into_item'      => __( 'Insert into podcast', 'church_core' ),
			'uploaded_to_this_item' => __( 'Uploaded to this podcast', 'church_core' ),
			
			
		),
	);
	
	register_post_type( 'podcast', apply_filters( 'church_core_podcast_params', $podcast_params ) );


// Register Message Series
	if ( $permalinks['series_permalink'] != 'series' ) {
		$series_slug = $permalinks['series_permalink'];
	} else {
		$series_slug = 'series';
	}

	register_taxonomy( 'series', 'podcast', 
		array( 
			'hierarchical' => true, 
				'labels' => array(
					'name' 				=> __( 'Series', 'church-builder' ),
					'singular_name' 	=> __( 'Series', 'church-builder' ),
					'search_items' 		=> __( 'Search Series', 'church-builder' ),
					'popular_items' 	=> __( 'Popular Series', 'church-builder' ),
					'all_items' 		=> __( 'All Series', 'church-builder' ),
					'parent_item' 		=> __( 'Parent Series', 'church-builder' ),
					'parent_item_colon' => __( 'Parent Series:', 'church-builder' ),
					'edit_item' 		=> __( 'Edit Series', 'church-builder' ),
					'update_item' 		=> __( 'Update Series', 'church-builder' ),
					'add_new_item' 		=> __( 'Add New Series', 'church-builder' ),
					'new_item_name' 	=> __( 'New Series Name', 'church-builder' ),
				), 
			'query_var' => true,
			'show_in_nav_menus' => true,
			'hierarchical' => true,
			'show_in_rest' => true,
			'rewrite' => array( 'slug' => $series_slug, 'with_front' => false )
		) 
	); 

// Register Speaker Type
	if ( $permalinks['speaker_permalink'] != 'speaker' ) {
		$speaker_slug = $permalinks['speaker_permalink'];
	} else {
		$speaker_slug = 'speaker';
	}
	
	register_taxonomy( 'speaker', 'podcast', 
		array( 
			'hierarchical' => true, 
				'labels' => array(
					'name' 				=> __( 'Speaker', 'church-builder' ),
					'singular_name' 	=> __( 'Speaker', 'church-builder' ),
					'search_items' 		=> __( 'Search Speakers', 'church-builder' ),
					'popular_items' 	=> __( 'Popular Speakers', 'church-builder' ),
					'all_items' 		=> __( 'All Speakers', 'church-builder' ),
					'parent_item' 		=> __( 'Parent Speakers', 'church-builder' ),
					'parent_item_colon' => __( 'Parent Speakers:', 'church-builder' ),
					'edit_item' 		=> __( 'Edit Speaker', 'church-builder' ),
					'update_item' 		=> __( 'Update Speaker', 'church-builder' ),
					'add_new_item' 		=> __( 'Add New Speaker', 'church-builder' ),
					'new_item_name' 	=> __( 'New Speaker Name', 'church-builder' ),
				), 
			'query_var' => true,
			'show_in_nav_menus' => true,
			'hierarchical' => true,
			'show_in_rest' => true,
			'rewrite' => array( 'slug' => $speaker_slug, 'with_front' => false )
		) 
	);
	
// Message Tags
	if ( $permalinks['tag_permalink'] != 'podcast-tag' ) {
		$tag_slug = $permalinks['tag_permalink'];
	} else {
		$tag_slug = 'podcast-tag';
	}
		
	register_taxonomy( 'podcast-tag', 'podcast', 
		array( 
			'hierarchical' => false, 
				'labels' => array(
					'name' 				=> __( 'Tags', 'church-builder' ),
					'singular_name' 	=> __( 'Tag', 'church-builder' ),
					'search_items' 		=> __( 'Search Tags', 'church-builder' ),
					'popular_items' 	=> __( 'Popular Tags', 'church-builder' ),
					'all_items' 		=> __( 'All Tags', 'church-builder' ),
					'parent_item' 		=> __( 'Parent Tags', 'church-builder' ),
					'parent_item_colon' => __( 'Parent Tags:', 'church-builder' ),
					'edit_item' 		=> __( 'Edit Tag', 'church-builder' ),
					'update_item' 		=> __( 'Update Tag', 'church-builder' ),
					'add_new_item' 		=> __( 'Add New Tag', 'church-builder' ),
					'new_item_name' 	=> __( 'New Tag Name', 'church-builder' ),
				), 
			'query_var' => true,
			'show_in_nav_menus' => true,
			'hierarchical' => true,
			'show_in_rest' => true,
			'rewrite' => array( 'slug' => $tag_slug, 'with_front' => false )
			) 
		);
	
	}
	add_action( 'init', 'church_core_podcast_register' );


// Add sorting for series
	function church_core_series_filter() {
	
	    // only display these taxonomy filters on desired custom post_type listings
	    global $typenow;
	    if ($typenow == 'podcast') {
	
	        // create an array of taxonomy slugs you want to filter by - if you want to retrieve all taxonomies, could use get_taxonomies() to build the list
	        $filters = array('series');
	
	        foreach ($filters as $tax_slug) {
	            // retrieve the taxonomy object
	            $tax_obj = get_taxonomy($tax_slug);
	            $tax_name = $tax_obj->labels->name;
	            // retrieve array of term objects per taxonomy
	            $terms = get_terms($tax_slug);
	
	            // output html for taxonomy dropdown filter
	            echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
	            echo "<option value=''>Show All $tax_name</option>";
	            foreach ($terms as $term) {
	                // output each select option line, check against the last $_GET to show the current option selected
	                echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
	            }
	            echo "</select>";
	        }
	    }
	}
	add_action( 'restrict_manage_posts', 'church_core_series_filter' );



// Add sorting for speakers
	function church_core_speaker_filter() {
	
	    // only display these taxonomy filters on desired custom post_type listings
	    global $typenow;
	    if ($typenow == 'podcast') {
	
	        // create an array of taxonomy slugs you want to filter by - if you want to retrieve all taxonomies, could use get_taxonomies() to build the list
	        $filters = array('speaker');
	
	        foreach ($filters as $tax_slug) {
	            // retrieve the taxonomy object
	            $tax_obj = get_taxonomy($tax_slug);
	            $tax_name = $tax_obj->labels->name;
	            // retrieve array of term objects per taxonomy
	            $terms = get_terms($tax_slug);
	
	            // output html for taxonomy dropdown filter
	            echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
	            echo "<option value=''>Show All $tax_name</option>";
	            foreach ($terms as $term) {
	                // output each select option line, check against the last $_GET to show the current option selected
	                echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
	            }
	            echo "</select>";
	        }
	    }
	}
	add_action( 'restrict_manage_posts', 'church_core_speaker_filter' );
	

// Add sorting for series
	function church_core_tag_filter() {
	
	    // only display these taxonomy filters on desired custom post_type listings
	    global $typenow;
	    if ($typenow == 'podcast') {
	
	        // create an array of taxonomy slugs you want to filter by - if you want to retrieve all taxonomies, could use get_taxonomies() to build the list
	        $filters = array('podcast-tag');
	
	        foreach ($filters as $tax_slug) {
	            // retrieve the taxonomy object
	            $tax_obj = get_taxonomy($tax_slug);
	            $tax_name = $tax_obj->labels->name;
	            // retrieve array of term objects per taxonomy
	            $terms = get_terms($tax_slug);
	
	            // output html for taxonomy dropdown filter
	            echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
	            echo "<option value=''>Show All $tax_name</option>";
	            foreach ($terms as $term) {
	                // output each select option line, check against the last $_GET to show the current option selected
	                echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
	            }
	            echo "</select>";
	        }
	    }
	}
	add_action( 'restrict_manage_posts', 'church_core_tag_filter' );
	

// Admin columns for podcast post type	
	function church_core_podcast_table_head( $defaults ) {
	
	    $defaults['featured_image']	= __( 'Graphic', 'church-builder' );
	    $defaults['speaker']		= __( 'Speaker', 'church-builder' );
	    $defaults['series']			= __( 'Series', 'church-builder' );
	    $defaults['podcast-tag']	= __( 'Tags', 'church-builder' );
	    
	    return $defaults;
	}
	add_filter( 'manage_podcast_posts_columns', 'church_core_podcast_table_head' );


// Add content for custom columns
	function church_core_podcast_table_content( $column_name, $post_id ) {
	    
	    if ( $column_name == 'featured_image' ) {
	    
	    	if ( '' != get_post_meta( $post_id, '_thumbnail_id', true ) ) {
	    
	    		$featured_image = get_post_meta( $post_id, '_thumbnail_id', true );
	    		
	    		$image_url = wp_get_attachment_url( $featured_image );
	    	
	    	} else if ( '' != get_the_terms( $post->ID, 'series' ) ) {
	    	
	    		foreach ( ( get_the_terms( $post->ID, 'series' ) ) as $term ) { 
	    		
	    			$term_list = $term->term_id. '';
	    			
	    			$term_title = $term->name. '';
	    			
	    			$image_id = get_term_meta( $term_list, 'image', true );
	    			
	    			$image_url = esc_url( $image_id );
	    		
	    		}
	    	
	    	}
    		
    		echo  '<img style="width: 100%; height: auto" src="';
    		echo $image_url;
    		echo '" />';
	    
	    }
	    
	    if ( $column_name == 'series' ) {
	    
	    	echo get_the_term_list( $post_id, 'series', '', ', ','' );
	    }
	    
	    if ( $column_name == 'speaker' ) {
	    
	    	echo get_the_term_list( $post_id, 'speaker', '', ', ','' );
	    }
	    
	    if ( $column_name == 'podcast-tag' ) {
	    
	    	echo get_the_term_list( $post_id, 'podcast-tag', '', ', ','' );
	    }
	
	}
	add_action( 'manage_podcast_posts_custom_column', 'church_core_podcast_table_content', 10, 2 );


// Admin columns for series taxonomy
	function church_core_series_columns( $new_columns ) {
	    $new_columns = array(
	        'cb' => '<input type="checkbox" />',
	        'name'   => __( 'Name', 'church-builder' ),
	        'posts'  => __( 'Posts', 'church-builder' ),
	        'graphic'  => __( 'Graphic', 'church-builder' ),
	        'description'  => __( 'Description', 'church-builder' )
	    );
	    return $new_columns;
	}
	add_filter( 'manage_edit-series_columns', 'church_core_series_columns' );
	add_filter( 'manage_edit-series_sortable_columns', 'church_core_series_columns' );
	
	
// Content for series admin columns
	function church_core_add_series_column_content( $content, $column_name, $term_id ){
	    
	    switch ( $column_name ) {
	    
	        case 'graphic':
	        
	        	if ( '' != get_term_meta( $term_id, 'image', true ) ) {
	        		
	        		$content = '<img style="max-width: 100%; height: auto;" src="'.get_term_meta( $term_id, 'image', true ).'" />';
	        	
	        	} else { 
	        		
	        		$content = '<img style="max-width: 100%; height: auto;" src="'.esc_url( get_template_directory_uri().'/images/series-placeholder.png' ).'" />';
	        		
	        	}            
	            
	            break;
	    
	    }
	    return $content;
	    
	}
	add_filter('manage_series_custom_column', 'church_core_add_series_column_content',10,3);
	

// add data to podcast rest api response
	function church_core_add_podcast_json_data( $data, $post, $request ) {
			
		$_data = $data->data;
		$thumbnail_id = get_post_thumbnail_id( $post->ID );
		$thumbnail = wp_get_attachment_image_src( $thumbnail_id, 'full' );

		$_data['featured_image_url'] = $thumbnail[0];

		foreach ( ( get_the_terms( $post->ID, 'series' ) ) as $term ) { 
			$term_list = $term->term_id. '';
			$term_title = $term->name. '';
			$image_id = get_term_meta( $term_list, 'image', true );
			$series_graphic = esc_url( $image_id );
		}

		$_data['series_graphic'] = $series_graphic;

		if ( ( $thumbnail[0] == '' ) || ( $thumbnail[0] == null ) ) {
			$episode_graphic = $series_graphic;
		} else {
			$episode_graphic = $thumbnail[0];
		}

		$_data['episode_graphic'] = $episode_graphic;
		$_data['nice_date'] = get_the_date();
		$_data['description'] =  wp_trim_words(get_the_excerpt(), 20, '...');

		$_data['speaker'] = wp_get_post_terms( get_the_ID(), 'speaker' );
		$current_series = wp_get_post_terms( get_the_ID(), 'series' );
		$_data['series'] = $current_series;
		$_data['mp3'] = get_post_meta($post->ID, 'message_audio', true );
		$_data['message_video'] = get_post_meta($post->ID, 'message_video', true );
		$_data['message_embed'] = get_post_meta($post->ID, 'video_embed', true );
		$_data['message_notes'] = get_post_meta($post->ID, 'message_notes', true );
		
		$i = 0;
		foreach (get_terms('series', array(
			'orderby'	=> 'id',
			'order'		=> 'DESC'
		)) as $list) {

			$term_id = $image_url = $series_slug = $series_image = $series_title = '';

			$term_id = $list->term_id;
			$image_url = esc_url( get_term_meta( $term_id, 'image', true ) );
            $series_slug = $list->slug;          	
			$series_image = $image_url;
			$series_title = $list->name;

			$series_array[] = array(
				'term_id' => $term_id,
				'series_slug' => $series_slug,
				'series_image' => $series_image,
				'series_title' => $series_title
			);

			$i++;
			if ( $i >= 4 ) {
				break;
			}

		}

		$_data['recent_series'] = $series_array;
		
		$custom_terms = wp_get_post_terms($post->ID, 'series');

		if( $custom_terms ){

			$tax_query = array();
	
			if( count( $custom_terms > 1 ) )
				$tax_query['relation'] = 'OR' ;
	
			foreach( $custom_terms as $custom_term ) {
	
				$tax_query[] = array(
					'taxonomy' => 'series',
					'field' => 'slug',
					'terms' => $custom_term->slug,
				);
	
			}
	
			$args = array( 
				'post_type' => 'podcast',
				'posts_per_page' => 20,
				'tax_query' => $tax_query 
			);

			$loop = new WP_Query($args);
	
			while( $loop->have_posts() ) : $loop->the_post();

				$episode_title = get_the_title();
				$episode_slug = basename(get_permalink());

				$other_episodes[] = array(
					'title' => $episode_title,
					'slug' => $episode_slug
				);

			endwhile;
	
			wp_reset_query();
		}

		$_data['other_episodes'] = $other_episodes;

		$data->data = $_data;
		return $data;

	}
	add_filter( 'rest_prepare_podcast', 'church_core_add_podcast_json_data', 10, 3 );


// add series graphic to rest api response
	function church_core_add_taxonomy_json_data( $data, $post, $request ) {
		$_data = $data->data;
		
		$image_id = get_term_meta( $post->term_id, 'image', true );
		$series_graphic = esc_url( $image_id );
		
		$_data['series_graphic'] = $series_graphic;

		$data->data = $_data;
		return $data;
	}
	add_filter( 'rest_prepare_series', 'church_core_add_taxonomy_json_data', 10, 3 );