<?php

// override files via plugin if theme doesn't declare church-core-override support
    function church_core_template_overrides( $template ) {
        
        $church_core_template_loader = new Church_Core_Template_Loader_Extension;

        if ( is_single() && get_post_type() == 'church-core-feed' ) {

            $church_core_template_loader->get_template_part( 'podcast-feed' );

        } else if ( ! current_theme_supports( 'church-core-overrides' ) ) {

            if ( is_single() && get_post_type() == 'podcast' ) {
                $church_core_template_loader->get_template_part( 'podcast-single' );
            } else if ( is_post_type_archive( 'podcast' ) ) {
                $church_core_template_loader->get_template_part( 'podcast-archive' );
            } else if ( is_tax( 'series' ) ) {
                $church_core_template_loader->get_template_part( 'taxonomy-series' );
            } else if ( is_tax( 'speaker' ) ) {
                $church_core_template_loader->get_template_part( 'taxonomy-speaker' );
            } else if ( is_tax( 'podcast-tag' ) ) {
                $church_core_template_loader->get_template_part( 'taxonomy-podcast-tag' );
            } else {
                return $template;
            }

        }
        
    }
    add_filter( 'template_include', 'church_core_template_overrides', 99, 1 );