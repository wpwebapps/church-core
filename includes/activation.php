<?php

// call post type registration on activation and then flush rewrite to prevent 404
    function church_core_push_default_settings() {
        
        $defaults = array(
            'image'             => esc_url( CHURCH_CORE_PLUGIN_DIR_URL .'assets/images/series-placeholder.png' ),
            'podcast_permalink' => 'podcast',
            'series_permalink'  => 'series',
            'speaker_permalink' => 'speaker',
            'tag_permalink'     => 'podcast-tag'
        );
        add_option( 'church_core_options', $defaults );

        church_core_podcast_register();
        church_core_feed_type_register();
        flush_rewrite_rules();

    }
    register_activation_hook( __FILE__, 'church_core_push_default_settings' );