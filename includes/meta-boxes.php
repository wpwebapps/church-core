<?php

/**
 * Adds field and meta boxes for various
 * places in the pages, posts, custom post types and users
 *
 * @package		WordPress
 * @subpackage	Church Builder
 * @since		1.0.0
 *
 */


// load cmb2 meta box class
	if ( file_exists( dirname( __FILE__ ) . '/meta-boxes/init.php' ) ) {
		require_once dirname( __FILE__ ) . '/meta-boxes/init.php';
	}

// Creates our user info metabox
	function church_core_speaker_metabox() {
		
		$speaker_information = new_cmb2_box( array(
			'id'               => 'speaker_metabox',
			'title'            => __( 'Speaker Information', 'church-core' ),
			'object_types'     => array( 'term' ), 
			'taxonomies'       => array( 'speaker' )
		) );
		
		$speaker_information->add_field( array(
			'name'    => __( 'Speaker Picture', 'church-core' ),
			'id'      => 'speaker_picture',
			'type'    => 'file',
		) );
	
	}

	add_action( 'cmb2_init', 'church_core_speaker_metabox' );


// Podcast Details
	function church_core_podcast_details() {
		
		$podcast_details = new_cmb2_box( array(
			'id'			=> 'podcast_details_metabox',
			'title'			=> __( 'Podcast Files', 'church-core' ),
			'object_types'	=> array( 'podcast' ),
			'context'		=> 'normal',
			'priority'      => 'high',
			'show_names'    => true,
		) );
		
		$podcast_details->add_field( array(
				'name' => __( 'Message Audio File', 'church-core' ),
				'desc' => __( 'Upload a file or enter an URL.', 'church-core' ),
				'id' => 'message_audio',
				'type' => 'file'
		) );
				
		$podcast_details->add_field( array(
				'name' => __( 'Message Notes', 'church-core' ),
				'desc' => __( 'Upload a file or enter an URL.', 'church-core' ),
				'id' => 'message_notes',
				'type' => 'file'
		) );
		
		$podcast_details->add_field( array(
				'name' => __( 'Message Video File', 'church-core' ),
				'desc' => __( 'Upload a file or enter an URL.', 'church-core' ),
				'id' => 'message_video',
				'type' => 'file'
		) );
		
		$podcast_details->add_field( array(
				'name' => __( 'Video Embed Code', 'church-core' ),
				'desc' => __( 'To stream your video from a service such as YouTube or Vimeo, paste the embed code here.', 'church-core' ),
				'id'   => 'video_embed',
				'type' => 'textarea_code',
		) );

		$podcast_details->add_field( array(
			'name' => __( 'RSS Keywords', 'church-core' ),
			'desc' => __( 'Separate keywords with commas.', 'church-core' ),
			'id' => 'message_keywords',
			'type' => 'text'
	) );
		
	}
	
	add_action( 'cmb2_init', 'church_core_podcast_details' );
	
	

// Feed Details box 
	function church_core_feed_details_metabox() {
		
		$feed_details = new_cmb2_box( array(
			'id'			=>  'church_builder_feed_details_metabox',
			'title'			=> __( 'Podcast Feed Details', 'church-core' ),
			'object_types'	=> array( 'church-core-feed' ),
			'context'		=> 'normal',
			'priority'      => 'high',
			'show_names'    => true,
		) );
		
		$feed_details->add_field( array(
				'name'    => __( 'Feed Type', 'church-core' ),
				'desc'    => __( 'Create separate feeds for audio and video episodes.', 'church-core' ),
				'id'      => 'feed-type',
				'type'    => 'select',
				    'options' => array(
				        'audio'	=> __( 'Audio', 'church-core' ),
				        'video'	=> __( 'Video', 'church-core' ),
				    ),
				'default' => '',
		) );

		$feed_details->add_field( array(
			'name' => __( 'Episodes In Feed', 'church-core' ),
			'desc' => __( 'How many episodes max to include in your feed.', 'church-core' ),
			'id' => 'per_page',
			'type' => 'text'
		) );
		
		$feed_details->add_field( array(
			'name' => __( 'Podcast Artwork', 'church-core' ),
			'desc' => __( 'Upload a file or enter an URL.  The recommended size is 3000 pixels square in PNG format.', 'church-core' ),
			'id' => 'podcast_art',
			'type' => 'file'
		) );
		
		$feed_details->add_field( array(
			'name' => __( 'Podcast Title', 'church-core' ),
			'desc' => __( 'The name of your podcast feed', 'church-core' ),
			'id' => 'podcast_title',
			'type' => 'text'
		) );
		
		$feed_details->add_field( array(
			'name' => __( 'Podcast Sub-Title', 'church-core' ),
			'desc' => __( 'The sub-title of your podcast feed', 'church-core' ),
			'id' => 'podcast_subtitle',
			'type' => 'text'
		) );
		
		$feed_details->add_field( array(
			'name' => __( 'Podcast Archive URL', 'church-core' ),
			'desc' => __( 'The URL to your podcast archive page.  Not this feed page, but the one your visitors see on the front end of your site.', 'church-core' ),
			'id' => 'podcast_url',
			'type' => 'text_url'
		) );
		
		$feed_details->add_field( array(
			'name' => __( 'Podcast Author', 'church-core' ),
			'desc' => __( 'The main author or speaker in your podcast.', 'church-core' ),
			'id' => 'podcast_author',
			'type' => 'text'
		) );
		
		$feed_details->add_field( array(
			'name' => __( 'Podcast Summary', 'church-core' ),
			'desc' => __( 'Write a short description of your podcast here.', 'church-core' ),
			'id' => 'podcast_summary',
			'type' => 'textarea'
		) );
		
		$feed_details->add_field( array(
			'name' => __( 'Podcast Owner', 'church-core' ),
			'desc' => __( 'The name of the podcast owner.', 'church-core' ),
			'id' => 'podcast_owner',
			'type' => 'text'
		) );
		
		$feed_details->add_field( array(
			'name' => __( 'Podcast Email', 'church-core' ),
			'desc' => __( 'This is the email address of your podcast owner.', 'church-core' ),
			'id' => 'podcast_email',
			'type' => 'text'
		) );
		
		$feed_details->add_field( array(
			'name' => __( 'iTunes Category', 'church-core' ),
			'desc'    => __( 'Choose a category for your podcast feed.', 'church-core' ),
			'id'      => 'podcast_category',
			'type'    => 'select',
				'options' => array(
					'' => '',
					'Arts'	=> __( 'Arts', 'church-core' ),
					'Business'	=> __( 'Business', 'church-core' ),
					'Comedy'	=> __( 'Comedy', 'church-core' ),
					'Education'	=> __( 'Education', 'church-core' ),
					'Games & Hobbies'	=> __( 'Games & Hobbies', 'church-core' ),
					'Government & Organizations'	=> __( 'Government & Organizations', 'church-core' ),
					'Health'	=> __( 'Health', 'church-core' ),
					'Music'	=> __( 'Music', 'church-core' ),
					'News & Politics'	=> __( 'News & Politics', 'church-core' ),
					'Religion & Spirituality'	=> __( 'Religion & Spirituality', 'church-core' ),
					'Science & Medicine'	=> __( 'Science & Medicine', 'church-core' ),
					'Society & Culture'	=> __( 'Society & Culture', 'church-core' ),
					'Sports & Recreation'	=> __( 'Sports & Recreation', 'church-core' ),
					'Technology'	=> __( 'Technology', 'church-core' ),
				),
			'default' => ''
		) );
		
		$feed_details->add_field( array(
			'name' => __( 'iTunes Sub-Category', 'church-core' ),
			'desc' => __( 'The selected sub-category must be under the appropriate main category.  You can\'t place "Self Help" under "Religion & Spirituality" for example since that sub-category is under "Health."', 'church-core' ),
			'id' => 'podcast_subcategory',
			'type' => 'select',
				'options' => array(
					'' => '',
					'Design'	=> __( 'Arts: Design', 'text-domain' ),
					'Fashion & Beauty'	=> __( 'Arts: Fashion & Beauty', 'text-domain' ),
					'Food'	=> __( 'Arts: Food', 'text-domain' ),
					'Literature'	=> __( 'Arts: Literature', 'text-domain' ),
					'Performing Arts'	=> __( 'Arts: Performing Arts', 'text-domain' ),
					'Spoken Word'	=> __( 'Arts: Spoken Word', 'text-domain' ),
					'Visual Arts'	=> __( 'Arts: Visual Arts', 'text-domain' ),
					
					'Business News'	=> __( 'Business: Business News', 'text-domain' ),
					'Careers'	=> __( 'Business: Careers', 'text-domain' ),
					'Investing'	=> __( 'Business: Investing', 'text-domain' ),
					'Management & Marketing'	=> __( 'Business: Management & Marketing', 'text-domain' ),
					'Shopping'	=> __( 'Business: Shopping', 'text-domain' ),
					
					'Educational Technology'	=> __( 'Education: Educational Technology', 'text-domain' ),
					'Higher Education'	=> __( 'Education: Higher Education', 'text-domain' ),
					'K-12'	=> __( 'Education: K-12', 'text-domain' ),
					'Language Courses'	=> __( 'Education: Language Courses', 'text-domain' ),
					'Training'	=> __( 'Education: Training', 'text-domain' ),
					
					'Automotive'	=> __( 'Games & Hobbies: Automotive', 'text-domain' ),
					'Aviation'	=> __( 'Games & Hobbies: Aviation', 'text-domain' ),
					'Hobbies'	=> __( 'Games & Hobbies: Hobbies', 'text-domain' ),
					'Other Games'	=> __( 'Games & Hobbies: Other Games', 'text-domain' ),
					'Video Games'	=> __( 'Games & Hobbies: Video Games', 'text-domain' ),
					
					'Local'	=> __( 'Government & Organizations: Local', 'text-domain' ),
					'National'	=> __( 'Government & Organizations: National', 'text-domain' ),
					'Non-Profit'	=> __( 'Government & Organizations: Non-Profit', 'text-domain' ),
					'Regional'	=> __( 'Government & Organizations: Regional', 'text-domain' ),
					
					'Alternative Health'	=> __( 'Health: Alternative Health', 'text-domain' ),
					'Fitness & Nutrition'	=> __( 'Health: Fitness & Nutrition', 'text-domain' ),
					'Self-Help'	=> __( 'Health: Self-Help', 'text-domain' ),
					'Sexuality'	=> __( 'Health: Sexuality', 'text-domain' ),
					'Kids & Family'	=> __( 'Health: Kids & Family', 'text-domain' ),
						
					'Alternative'	=> __( 'Music: Alternative', 'text-domain' ),
					'Blues'	=> __( 'Music: Blues', 'text-domain' ),
					'Country'	=> __( 'Music: Country', 'text-domain' ),
					'Easy Listening'	=> __( 'Music: Easy Listening', 'text-domain' ),
					'Electronic'	=> __( 'Music: Electronic', 'text-domain' ),
					'Folk'	=> __( 'Music: Folk', 'text-domain' ),
					'Freeform'	=> __( 'Music: Freeform', 'text-domain' ),
					'Hip-Hop & Rap'	=> __( 'Music: Hip-Hop & Rap', 'text-domain' ),
					'Inspirational'	=> __( 'Music: Inspirational', 'text-domain' ),
					'Jazz'	=> __( 'Music: Jazz', 'text-domain' ),
					'Latin'	=> __( 'Music: Latin', 'text-domain' ),
					'Metal'	=> __( 'Music: Metal', 'text-domain' ),
					'New Age'	=> __( 'Music: New Age', 'text-domain' ),
					'Oldies'	=> __( 'Music: Oldies', 'text-domain' ),
					'Pop'	=> __( 'Music: Pop', 'text-domain' ),
					'R&B & Urban'	=> __( 'Music: R&B & Urban', 'text-domain' ),
					'Reggae'	=> __( 'Music: Reggae', 'text-domain' ),
					'Rock'	=> __( 'Music: Rock', 'text-domain' ),
					'Seasonal & Holiday'	=> __( 'Music: Seasonal & Holiday', 'text-domain' ),
					'Soundtracks'	=> __( 'Music: Soundtracks', 'text-domain' ),
					'World'	=> __( 'Music: World', 'text-domain' ),
					
					'Conservative (Right)'	=> __( 'News & Politics: Conservative (Right)', 'text-domain' ),
					'Liberal (Left)'	=> __( 'News & Politics: Liberal (Left)', 'text-domain' ),
					
					'Buddhism'	=> __( 'Religion & Spirituality: Buddhism', 'text-domain' ),
					'Christianity'	=> __( 'Religion & Spirituality: Christianity', 'text-domain' ),
					'Hinduism'	=> __( 'Religion & Spirituality: Hinduism', 'text-domain' ),
					'Islam'	=> __( 'Religion & Spirituality: Islam', 'text-domain' ),
					'Judaism'	=> __( 'Religion & Spirituality: Judaism', 'text-domain' ),
					'Other'	=> __( 'Religion & Spirituality: Other', 'text-domain' ),
					'Spirituality'	=> __( 'Religion & Spirituality: Spirituality', 'text-domain' ),
					
					'Medicine'	=> __( 'Science & Medicine: Medicine', 'text-domain' ),
					'Natural Sciences'	=> __( 'Science & Medicine: Natural Sciences', 'text-domain' ),
					'Social Sciences'	=> __( 'Science & Medicine: Social Sciences', 'text-domain' ),
					
					'Gay & Lesbian'	=> __( 'Society & Culture: Gay & Lesbian', 'text-domain' ),
					'History'	=> __( 'Society & Culture: History', 'text-domain' ),
					'Personal Journals'	=> __( 'Society & Culture: Personal Journals', 'text-domain' ),
					'Philosophy'	=> __( 'Society & Culture: Philosophy', 'text-domain' ),
					'Places & Travel'	=> __( 'Society & Culture: Places & Travel', 'text-domain' ),
					
					'Amateur'	=> __( 'Sports & Recreation: Amateur', 'text-domain' ),
					'College & High School'	=> __( 'Sports & Recreation: College & High School', 'text-domain' ),
					'Outdoor'	=> __( 'Sports & Recreation: Outdoor', 'text-domain' ),
					'Professional'	=> __( 'Sports & Recreation: Professional', 'text-domain' ),
					
					'Gadgets'	=> __( 'Technology: Gadgets', 'text-domain' ),
					'IT News'	=> __( 'Technology: IT News', 'text-domain' ),
					'Podcasting'	=> __( 'Technology: Podcasting', 'text-domain' ),
					'Software How-To'	=> __( 'Technology: Software How-To', 'text-domain' ),
						
				),
				'default' => ''
		) );
		
		$feed_details->add_field( array(
			'name'    => __( 'Explicit', 'church-core' ),
			'desc'    => __( 'Designate the feed as having explicit content.', 'church-core' ),
			'id'      => 'podcast_explicit',
			'type'    => 'select',
				'options' => array(
					'no'	=> __( 'No', 'church-core' ),
					'yes'	=> __( 'Yes', 'church-core' ),
				),
			'default' => 'no'
		) );
	
	}

	add_action( 'cmb2_init', 'church_core_feed_details_metabox' );	


// Series image
	function church_core_series_image_metabox() {
	
		$series_metabox = new_cmb2_box( array(
			'id'               => 'series_metabox',
			'title'            => __( 'Series Metabox', 'church-core' ),
			'object_types'     => array( 'term' ), 
			'taxonomies'       => array( 'series' )
		) );
	
		$series_metabox->add_field( array(
			'name' => __( 'Series Image', 'church-core' ),
			'desc' => '',
			'id'   => 'image',
			'type' => 'file',
		) );
	
	}

	add_action( 'cmb2_admin_init', 'church_core_series_image_metabox' );


// Church Core Settings

	function church_core_settings_page_metabox() {

		$cc_settings = new_cmb2_box( array(
			'id'           => 'church_core_options',
			'title'        => esc_html__( 'Church Core Settings', 'church-core' ),
			'object_types' => array( 'options-page' ),
			'option_key'      => 'church_core_options',
			'menu_title'      => esc_html__( 'Settings', 'cmb2' ),
			'parent_slug'     => 'edit.php?post_type=podcast',
			'capability'      => 'manage_options'
		) );
		$cc_settings->add_field( array(
			'name' => esc_html__( 'Default Podcast Image', 'church-core' ),
			'desc' => esc_html__( 'This is displayed when no series or individual episode graphic is added', 'church-core' ),
			'default' => esc_url( CHURCH_CORE_PLUGIN_DIR_URL .'assets/images/series-placeholder.png' ),
			'id'   => 'image',
			'type' => 'file',
		) );
		$cc_settings->add_field( array(
			'name' => esc_html__( 'Podcast Permalink', 'church-core' ),
			'desc' => esc_html__( 'Individual podcast episode slug', 'church-core' ),
			'id'   => 'podcast_permalik',
			'default' => 'podcast',
			'type' => 'text'
		) );
		$cc_settings->add_field( array(
			'name' => esc_html__( 'Series Permalink', 'church-core' ),
			'desc' => esc_html__( 'Series archive slug', 'church-core' ),
			'id'   => 'series_permalink',
			'default' => 'series',
			'type' => 'text',
		) );
		$cc_settings->add_field( array(
			'name' => esc_html__( 'Speaker Permalink', 'church-core' ),
			'desc' => esc_html__( 'Speaker archive slug', 'church-core' ),
			'id'   => 'series_permalink',
			'default' => 'speaker',
			'type' => 'text',
		) );
		$cc_settings->add_field( array(
			'name' => esc_html__( 'Tag Permalink', 'church-core' ),
			'desc' => esc_html__( 'Tag archive slug', 'church-core' ),
			'id'   => 'tag_permalink',
			'default' => 'podcast-tag',
			'type' => 'text',
		) );

	}
	add_action( 'cmb2_admin_init', 'church_core_settings_page_metabox' );