<?php
/**
 * Shortcodes
 *
 * Creates shortcodes for use with the plugin.  The podcast 
 * item and pagination are intended for use inside of 
 * template files and not post content.
 *
 * @package		WP Web Apps
 * @subpackage	Church Core
 * @since		1.0.0
*/ 

// Podcast Search		
	function church_core_podcast_search_shortcode( $atts, $content = null ) {
	   
		$templates = new Church_Core_Template_Loader_Extension;
		
		ob_start();
		
		$templates->get_template_part( 'podcast-searchform' );
		
		return ob_get_clean();
	   
	}
	add_shortcode('podcast-search', 'church_core_podcast_search_shortcode');


// Podcast Item		
	function church_core_podcast_item_shortcode( $atts, $content = null ) {
	   
		$templates = new Church_Core_Template_Loader_Extension;
		
		ob_start();
		
		$templates->get_template_part( 'podcast-item' );
		
		return ob_get_clean();
	   
	}
	add_shortcode('podcast-item', 'church_core_podcast_item_shortcode');


// Pagination	
	function church_core_pagination_shortcode( $atts, $content = null ) {
		
		$templates = new Church_Core_Template_Loader_Extension;
		
		ob_start();
		
		$templates->get_template_part( 'podcast-pagination' );
		
		return ob_get_clean();
	
	}
	add_shortcode('podcast-pagination', 'church_core_pagination_shortcode');


// Series Archive	
	function church_core_series_archive_shortcode( $atts, $content = null ) {
			
		$templates = new Church_Core_Template_Loader_Extension;
		
		ob_start();
		
		$templates->get_template_part( 'podcast-series-archive' );
		
		return ob_get_clean();

	}
	add_shortcode('podcast-series-archive', 'church_core_series_archive_shortcode');