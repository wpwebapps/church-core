<?php


class ChurchCoreSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_submenu_page(
            'edit.php?post_type=podcast',
            __( 'Podcast Settings', 'church-core' ),
            __( 'Settings', 'church-core' ),
            'manage_options', 
            'church-core-settings', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {

        // Set class property

        if ( !get_option( 'church_core' ) ) {
            $defaults = array(
                'podcast' => 'podcast',
                'series'  => 'series',
                'speaker' => 'speaker',
                'tag'    => 'podcast-tag'
            );
            add_option('church_core', $defaults);
        }

        $this->options = get_option( 'church_core' );

        ?>
        <div class="wrap">
            <h1><?php echo __( 'Podcast Settings', 'church-core' ); ?></h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'church_core_option_group' );
                do_settings_sections( 'church-core-settings' );
                submit_button();
                flush_rewrite_rules();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'church_core_option_group', // Option group
            'church_core', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'church_core_permalink_section', // ID
            'Permalinks', // Title
            array( $this, 'print_section_info' ), // Callback
            'church-core-settings' // Page
        );
        
        add_settings_field(
            'podcast', 
            'Single Podcast', 
            array( $this, 'podcast_callback' ), 
            'church-core-settings', 
            'church_core_permalink_section'
        );

        add_settings_field(
            'series', 
            'Series', 
            array( $this, 'series_callback' ), 
            'church-core-settings', 
            'church_core_permalink_section'
        );   
        
        add_settings_field(
            'speaker', 
            'Speaker', 
            array( $this, 'speaker_callback' ), 
            'church-core-settings', 
            'church_core_permalink_section'
        );

        add_settings_field(
            'tag', 
            'Podcast Tag', 
            array( $this, 'tag_callback' ), 
            'church-core-settings', 
            'church_core_permalink_section'
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();

        if( isset( $input['podcast'] ) )
            $new_input['podcast'] = sanitize_text_field( $input['podcast'] );

        if( isset( $input['series'] ) )
            $new_input['series'] = sanitize_text_field( $input['series'] );
        
        if( isset( $input['speaker'] ) )
            $new_input['speaker'] = sanitize_text_field( $input['speaker'] );
        
        if( isset( $input['tag'] ) )
            $new_input['tag'] = sanitize_text_field( $input['tag'] );

        return $new_input;

    }

    // section description
    public function print_section_info()
    {
        print 'You can customize the URL struture of your podcasts here.  Make sure to use <em>all lowercase characters and no spaces.</em>';
    }

    

// Creates markup for our inputs
    public function podcast_callback()
    {
        printf(
            '<input type="text" class="regular-text code" id="podcast" name="church_core[podcast]" value="%s" />',
            isset( $this->options['podcast'] ) ? esc_attr( $this->options['podcast']) : ''
        );
    }

    public function series_callback()
    {
        printf(
            '<input type="text" class="regular-text code" id="series" name="church_core[series]" value="%s" />',
            isset( $this->options['series'] ) ? esc_attr( $this->options['series']) : ''
        );
    }
    public function speaker_callback()
    {
        printf(
            '<input type="text" class="regular-text code" id="speaker" name="church_core[speaker]" value="%s" />',
            isset( $this->options['speaker'] ) ? esc_attr( $this->options['speaker']) : ''
        );
    }
    public function tag_callback()
    {
        printf(
            '<input type="text" class="regular-text code" id="tag" name="church_core[tag]" value="%s" />',
            isset( $this->options['tag'] ) ? esc_attr( $this->options['tag']) : ''
        );
    }
}

if( is_admin() )
    $church_core_settings_page = new ChurchCoreSettingsPage();