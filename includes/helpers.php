<?php


// Date function for podcast feed
    function church_core_rss_date( $timestamp = null ) {
        $timestamp = ($timestamp==null) ? time() : $timestamp;
        echo date(DATE_RSS, $timestamp);
    }

// Reverse Escape Function For Podcast Categories
    function church_core_reverse_escape($str) {
        
        $search=array("\\\\","\\0","\\n","\\r","\Z","\'",'\"');
        $replace=array("\\","\0","\n","\r","\x1a","'",'"');
        return str_replace($search,$replace,$str);

    }

// Remove any more tags from excerpts in podcasts for our feed
    function church_core_excerpt_more_tag($output) {
        
        global $post;
        if ( $post->post_type == 'podcast' ) {
            $output = '';  
        }
        return $output;

    }
    add_filter( 'excerpt_more', 'church_core_excerpt_more_tag', 100 );

// Get attachment ID from image URL from https://pippinsplugins.com/retrieve-attachment-id-from-image-url/
    function church_core_get_image_id($image_url) {
        global $wpdb;
        
        $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
            return $attachment[0]; 
    }