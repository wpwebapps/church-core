<?php

/**
 * Adds support for our podcasting
 * post type and the message/speaker taxonomies
 *
 * @package		WordPress
 * @subpackage	Church Core
 * @since		1.0.0
 *
 */

// Register Feed Post Type
	function church_core_feed_type_register() {
		
		if ( post_type_exists( 'church-core-feed' ) ) {
			return;
		}
		
		$church_core_feed_args = array(
			'label'                 => __( 'Feed', 'church_core' ),
			'description'           => __( 'Create RSS feeds for your podcasts', 'church_core' ),
			'supports'              => array( 'title', 'custom-fields', ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => 'edit.php?post_type=podcast',
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-rss',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => true,		
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'rewrite' 				=> array( 'slug' => 'podcast-feed', 'with_front' => false ),
			'labels' 				=> array(
				'name'                  => _x( 'Feeds', 'Post Type General Name', 'church_core' ),
				'singular_name'         => _x( 'Feed', 'Post Type Singular Name', 'church_core' ),
				'menu_name'             => __( 'Feed', 'church_core' ),
				'name_admin_bar'        => __( 'Feed', 'church_core' ),
				'archives'              => __( 'Feed Archives', 'church_core' ),
				'parent_item_colon'     => __( 'Feed Parent', 'church_core' ),
				'all_items'             => __( 'Feeds', 'church_core' ),
				'add_new_item'          => __( 'Add New Feed', 'church_core' ),
				'add_new'               => __( 'Add Feed', 'church_core' ),
				'new_item'              => __( 'New Feed', 'church_core' ),
				'edit_item'             => __( 'Edit Feed', 'church_core' ),
				'update_item'           => __( 'Update Feed', 'church_core' ),
				'view_item'             => __( 'View Feed', 'church_core' ),
				'search_items'          => __( 'Search Feed', 'church_core' ),
				'not_found'             => __( 'No feeds', 'church_core' ),
				'not_found_in_trash'    => __( 'No feeds in the trash', 'church_core' ),
			),
		);
		
		register_post_type( 'church-core-feed', apply_filters( 'church_core_feed_params', $church_core_feed_args ) );
	
}
add_action( 'init', 'church_core_feed_type_register', 0 );

