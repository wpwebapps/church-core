<?php

/**
 * Plugin Name:       Church Core
 * Plugin URI:        
 * Description:       An extensable plugin for managing and displaying your church podcasts.
 * Version:           0.9.0
 * Author:            WP Web Apps
 * Author URI:        https://wpwebapps.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       plugin-name
 * Domain Path:       /languages
 */


// If this file is called directly, abort.
	if ( ! defined( 'WPINC' ) ) {
		die;
	}

// Define some constants
	define( 'CHURCH_CORE_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
	define( 'CHURCH_CORE_PLUGIN_DIR_URL', plugin_dir_url( __FILE__) );
	define( 'CHURCH_CORE_PLUGIN_VERSION', '1.0.0' );

// Load podcast post type and taxonomies	
	include CHURCH_CORE_PLUGIN_DIR . 'includes/feed-post-type.php';
	include CHURCH_CORE_PLUGIN_DIR . 'includes/podcast-post-type.php';
		
// Load meta boxes for podcast and taxonomy fields
	include CHURCH_CORE_PLUGIN_DIR . 'includes/meta-boxes.php';
	
// activation functions
	include CHURCH_CORE_PLUGIN_DIR . 'includes/activation.php';

// load shortcodes
	include CHURCH_CORE_PLUGIN_DIR . 'includes/shortcodes.php';

// load helper functions
	include CHURCH_CORE_PLUGIN_DIR . 'includes/helpers.php';


// Template loader
	if ( ! class_exists( 'Gamajo_Template_Loader' ) ) {
		require CHURCH_CORE_PLUGIN_DIR . '/includes/template-loading/gamajo-template-loader.php';
	}
	require CHURCH_CORE_PLUGIN_DIR . '/includes/template-loading/template-loader-start.php';
	require CHURCH_CORE_PLUGIN_DIR . '/includes/template-loading/template-overrides.php';

// Load front end stylesheet
	function church_core_styleheet_loading() {

		if ( ! current_theme_supports( 'church-core-overrides' ) ) {
			wp_enqueue_style('church-core', CHURCH_CORE_PLUGIN_DIR_URL . 'assets/church-core-front.css', array(), CHURCH_CORE_PLUGIN_VERSION, 'all');
		}

	}
	add_action( 'wp_enqueue_scripts', 'church_core_styleheet_loading' );